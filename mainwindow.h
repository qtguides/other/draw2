#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPoint>

namespace Ui {
class MainWindow;
}

typedef enum {
    EstadoDibujo_Nada,
    EstadoDibujo_Dibujando,
    EstadoDibujo_Moviendo,
} EstadoDibujo_t;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

private:
    Ui::MainWindow *ui;

    QPoint startMovePt;    // Punto anterior del mouse al mover la linea
    EstadoDibujo_t estado; // Estado de dibujo en el que se encuentra la aplicación

    QLine line;            // Linea que se dibuja con el mouse
    int penWidth;          // Ancho del lapiz con el que se dibuja la linea
    QColor penColor;       // Color

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
};

#endif // MAINWINDOW_H
