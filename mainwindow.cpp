#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QMouseEvent>
#include<QPainter>
#include<QDebug>

/**
 * @brief Determina si un punto está sobre una linea (mas un "delta" de 8 pixeles).
 *
 * @param point Punto a testear
 * @param line  Linea a testear
 *
 * @return true si el punto está sobre la linea; en caso contrario false
 */
bool isPointInLine(const QPoint& point, QLineF line)
{
    QPointF intersectPnt;
    QLineF ptLine(point.x()-4, point.y()-4, point.x()+4, point.y()+4);

    return (line.intersect(ptLine, &intersectPnt) == QLineF::BoundedIntersection);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    estado = EstadoDibujo_Nada; // Inicialmente la aplicación está en estado IDLE, sin dibujar
    line.setLine(0, 0, 0, 0);   // Inicializa la linea
    penWidth = 2;
    penColor = Qt::red;
    ui->setupUi(this);

    // para capturar onMouseMove() aun cuando no haya boton presionado
    //setCentralWidget(nullptr); // anula el widget central (en realidad habria que
                                 // crear un widget y setearlo en el centro)
    centralWidget()->setAttribute(Qt::WA_TransparentForMouseEvents); // idem anterior
    setMouseTracking(true);    // activa el mmouse tracking en este widget/ventana
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Evento que se recibe cuando el usuario presiona el botón izquierdo sobre la ventana
void MainWindow::mousePressEvent(QMouseEvent *event)
{
     // si se presiono boton izquierdo
    if (event->button() == Qt::LeftButton)
    {
        // Determina si el mouse está sobre la linea
        QLineF linef(line);
        if(isPointInLine(event->pos(),linef))
        {
            // si esta sobre la linea, cambia mouse icon y prepararse para mover
            QApplication::setOverrideCursor(QCursor(Qt::SizeAllCursor));
            startMovePt = event->pos();
            estado = EstadoDibujo_Moviendo;
        } else {
            // si no esta sobre la linea, crea una linea nueva y prepara para dibujar
            QApplication::setOverrideCursor(QCursor(Qt::PointingHandCursor));
            line.setP1(event->pos());
            line.setP2(event->pos());
            estado = EstadoDibujo_Dibujando;
        }
    }

    // Llama al codigo de Qt para este evento
    QMainWindow::mousePressEvent(event);
}

// Evento que se recibe cuando el usuario mueve el mouse sobre la ventana
// (con o sin boton presionado, ya que llamamos a setMouseTracking(true) en el constructor)
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    // si el mouse se mueve con boton izquierdo presionado y esta dibujando
    if ((event->buttons() & Qt::LeftButton) && (estado == EstadoDibujo_Dibujando))
    {
        // actualiza 2do punto de la linea y solicita el repintado de la pantalla
        line.setP2(event->pos());
        update();
    }
         // si el mouse se mueve con boton izquierdo presionado y esta moviendo
    else if ((event->buttons() & Qt::LeftButton) && (estado == EstadoDibujo_Moviendo))
    {
        // calcula el offset de cuánto se movió el mouse con respecto al punto anterior
        QPoint offset(event->pos() - startMovePt);
        line.translate(offset); // traslada la linea ese offset calculado
        startMovePt = event->pos(); // setea el punto actual como punto anterior (p/ el futuro)
        update(); // solicita repintado de la pantalla
    }
    else // el mouse se movió sin botones apretados
    {
        // determina si el mouse está la linea, y cambia el icono del mouse
        QLineF linef(line);
        if(isPointInLine(event->pos(), linef))
        {
            QApplication::setOverrideCursor(QCursor(Qt::SizeAllCursor));
        } else {
            QApplication::restoreOverrideCursor();
        }
    }

    // Llama al codigo de Qt para este evento
    QMainWindow::mouseMoveEvent(event);
}

// Evento que se recibe cuando el usuario libera el botono del mouse
void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    // si se liberó el boton izq del mouse y esta dibujando
    if (event->button() == Qt::LeftButton && estado == EstadoDibujo_Dibujando)
    {
        QApplication::restoreOverrideCursor();
        line.setP2(event->pos());
        estado = EstadoDibujo_Nada;
        update();
    }

    // Llama al codigo de Qt para este evento
    QMainWindow::mouseReleaseEvent(event);
}

// Evento que se recibe cada vez que haya que repintar la ventana
// Ya sea  porque otra ventana sobre esta se movió, cambió eldibujo, o se movió de lugar esta ventana, etc
void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(QPen(penColor, penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawLine(line);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // Llama al codigo de Qt para este evento
    QMainWindow::paintEvent(event);
}
